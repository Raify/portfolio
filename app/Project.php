<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
   protected $fillable = [

    'title',
    'description',
	'photo'
  ];
  public function photos(){
    	return $this->hasMany('App\Photo');
	}
}
