<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
//visada ivykdoma coknstruktoriaus f-ja, bet ji buna be filtru, o mes norim padaryti, kad __construct visada ivykdytu 

    public function __construct(){
        $this->middleware(function ($request, $next){
            if(\Auth::guest()
                ||
                (!\Auth::guest() &&\Auth::user()->is_admin == 0))
                {
                    return redirect()->route('login');
                }

            return $next($request);
        });
}
//        if($user=Auth::user()) instanceOF User && $user->is_admin) {
//           echo "labas";
//       }
     
    
    public function index(Request $request)
    { 
        $users = \App\User::all();
 //       var_dump($users);
        return view('user.list',compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

            if(Auth::guest()){
                return redirect('/login');
            } else{
                return view('user.create', compact('user'));
            }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $newuser =  \App\User::create($request->all());   
   return redirect()->route('user.list');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
          $user = \App\User::find($id);
        return view('user.create',compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
  //  public function update(Request $request, $id)
    //{
     //   $data = $request->all();
     //   $data['password'] = \Hash::make($request->password);

     //   \App\User::find($id)->update($data);
     //   return redirerct()->('users.index');

//-----
          public function update(Request $request, $id)
    {
        // issaugom formos reiksmes, kad galetume modifkuoti
        $data = $request->all();
        
        if(!empty($request->password)){
            // jei password laukas nera tuscias - perrasom masyvo 'password' reiksme su jo pacio reiksme tik uzhashinta
            $data['password'] = \Hash::make($request->password);
        }else{
            // ismetam 'password' key is masyvo, nes kitaip uzsaugotu kaip tuscia password'a
            unset($data['password']);
        }
        
        // atnaujinam user duomenis su modifikuotais duomenimis
        $user = \App\User::find($id)->update($data);

        return redirect()->route('user.index');
    }



 //    $newuser=\App\User::find($id)->update($request->all());

   // return redirect()->route('user.index');
   // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      \App\User::find($id)->delete();
        return redirect()->route('user.index');
    }
}