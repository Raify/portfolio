<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('users')->insert([
          'name' => 'User',
          'email' => 'info@3wa.lt',
          'password' => bcrypt('password'),
          
        ]);
    }
}
