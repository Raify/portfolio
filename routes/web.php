<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/


Route::get('/', function () {
   return view('welcome', compact('menus'));
})->name('homepage'); 


Auth::routes();

Route::resource('/home', 'HomeController');
Route::resource('/team', 'TeamController');
Route::resource('/contacts', 'ContactController');


Auth::routes();

Route::resource('/photos','PhotoController'); 
Route::resource('/projects','ProjectController'); 
Route::resource('/users','UserController'); 

Route::get('file','TestController@index');
Route::post('store','TestController@store');






//Route::resource('users/uploadfile','FileUploadController');
//Route::post('users/uploads','FileUploadController@showfileupload');

//Route::post('/upload','UploadFileController@upload');

