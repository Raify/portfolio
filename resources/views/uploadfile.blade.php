
@extends('layouts.main')

@section('content')

@if(count($errors))
    <ul>
        @foreach($errors as $error)
            <li>{{error}}</li>
        @endforeach
    </ul>
@endif

@if($message = Session::get('success'))
    <p>{{message}}</p>
    <img src="/photos/{{Session::get('path')}}">
@endif  

<form method="POST" enctype="multipart/form-data" action="/upload">
    {{csrf_field()}}
    <input type="file" name="photo">
    <input type="submit" value="upload photo">
</form>             
@endsection