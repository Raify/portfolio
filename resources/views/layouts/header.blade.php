<!doctype html>
<html lang="en" class="no-js">
    <head>
       <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1">

        <link href="{{ asset ('http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700')}}" rel='stylesheet' type='text/css'>

        <link href="{{ asset('css/reset.css') }}" rel="stylesheet">

        <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style.css') }}" rel="stylesheet">
        <script src="{{asset('js/modernizr.js')}}"></script>
        <!-- Modernizr -->
    
       <title>3D k-light</title>
    </head>
<body>
  <header class="cd-header">
    <a href="#0" class="cd-logo"><img src="img/logo.png" alt="Logo"></a>
    <span class="cd-logo"><h1>Photo hunt - Explore Lithuania</h1></span>
    <a href="#0" class="cd-3d-nav-trigger">

      <span></span>
    </a>
  </header> <!-- .cd-header -->

  <main>
    <h1></h1>
    <img scr="img/">
    <!-- all your content here -->
  </main>
  
  <nav class="cd-3d-nav-container">
    <ul class="cd-3d-nav">
      <li class="cd-selected">
        <a href="{{ route('home.index')}}">Home</a>
      </li>

      <li>
        <a href="{{ route('projects.index')}}">Projects</a>
      </li>

      <li>
        <a href="{{ route('photos.index')}}">Gallery</a>
      </li>

      <li>
        <a href="{{ route('team.index')}}">Team</a>
      </li>

      <li>
        <a href="{{ route('contacts.index')}}">Contact Us</a>
      </li>
    </ul> <!-- .cd-3d-nav -->

    <span class="cd-marker color-1"></span> 
  </nav> <!-- .cd-3d-nav-container -->
