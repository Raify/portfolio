@extends("layouts.main")

@section("content")


<div class="container">

@if(isset($photo))

    <form action="{{route('photos.update', $photo->id)}}" method=POST>
        <input type="hidden" name="_method" value="PUT">
@else
    <form method=POST action="{{route('photos.store', $photo->id)}}">
@endif
        {{ csrf_field() }}


        <label> Project:</label>
                    <select name="project_id">
                        @foreach($projects as $project)
                                @if(isset ($photo) && $photo->project_id==$project->id)
                                <option selected value="{{ $project->id }}">{{ $project->title }}</option>
                                @else
                                <option value="{{ $menu->id }}">{{ $project->title }}</option>
                                @endif

                        @endforeach
                    </select><br>
        <label> Project:</label><br>
        @if(isset($photo))
        <input   value="{{$photo['project_id']}}" class="form-control thumbnail" type="text" name="number" ><br>
        @else

        <input   class="form-control thumbnail" type="text" name="number" ><br>
        @endif

        <label>Photo URL:</label><br>

        @if(isset($photo))
        <input  value="{{$photo['path']}}" class="form-control thumbnail" type="text" name="photo">
        @else
        <input class="form-control thumbnail" type="text" name="photo">
        @endif
        

        <button class="btn btn-primary">Add photo</button>

</form> 

<div>
<!-- Paslepta forma -->
@if(isset($photo))
<form method=POST action="{{route('photos.destroy', $photo->id)}}">

<input type="hidden" name="_method" value="DELETE">
{{ csrf_field() }}
<button class="btn btn-danger">Delete</button>
</form>
@endif

@endsection
    

