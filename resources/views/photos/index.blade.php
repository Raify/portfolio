@extends("layouts.main")
    
@section("content")

@yield('content')
	    	

<div class="container">
	<div class="row">
		<h1>Gallery</h1>
		<h2></h2>
	</div>
	
	<div class="row img-thumbnails">
	    <div class="col-md-12">
	        </a>
	        	@foreach($photos as $photo)
					<ul>
				 		 <li>{{ $photo['project_id']}}</li>
				 		 <li><img src="{{$photo['path']}}" class="img-thumbnail"></li>
				 		
				<li><a href="{{route('photos.edit', $photo->id)}}">Edit</a></li>
				
					</ul>	 	
					
					@endforeach
	    </div>
	    
   
<a href="{{ route('photos.create') }}"  class="btn btn-primary btn-small">Add New Photo</a>


</body>
@endsection





						
				

				
	