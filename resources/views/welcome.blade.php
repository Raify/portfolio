@extends("layouts.main")
    
@section("content")

	 @if (Auth::guest())
<div class="container">
	
	<div class="row img-thumbnails">
	    <div class="col-md-6">
	        <a href="#">
	            <img src="photo_uploads/city1.jpg" class="img-thumbnail">
	        </a>
	    </div>
	    <div class="col-md-6">
	        <a href="#">
	            <img src="photo_uploads/angles3.jpg" class="img-thumbnail">
	        </a>
	    </div>
	</div><br>
	
	<div class="row img-thumbnails">
	    <div class="col-md-4">
	        <a href="#">
	            <img src="photo_uploads/nature3.jpg" class="img-thumbnail">
	        </a>
	    </div>
	    <div class="col-md-4">
	        <a href="#">
	            <img src="photo_uploads/angles2.jpg" class="img-thumbnail">
	        </a>
	    </div>
	    <div class="col-md-4">
	        <a href="#">
	            <img src="photo_uploads/nature2.jpg" class="img-thumbnail">
	        </a>
	    </div>
	</div>
	
</div>
   @else
   <div class="container">

    <div class="row">
        
        <div class="col-md-12 ">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    Welcome {{ Auth::user()->name }}
                </div>
                </div>
            </div>
        </div>
    </div>
</div>   
   @endif


</body>
@endsection