@extends('layouts.main')

@section('content')


  <div class="container">
    
    <div class="row">
      <div class="col-md-6 col-sm-12">
      				<h1>Exhibitions</h1><br>
      				<h2>National Gallery of Art<h2>  
                    <h3>Working hours</h3>
                    <p>I-V 9:00-17:00</p>

                    <h3>Information</h3>
                    <p>+370 67 111 340</p>
      </div>
      <div class="col-md-6 col-sm-12">
        <div>
          <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2305.724301377392!2d25.26853631545471!3d54.69687908039532!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46dd9400c96f7d99%3A0xb0e073fc9079da4c!2sNacionalin%C4%97+Dail%C4%97s+Galerija!5e0!3m2!1slt!2slt!4v1481793919936" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
      </div>
    </div>
</div>
@endsection