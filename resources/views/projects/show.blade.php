@extends("layouts.main")

@section("content")
    <div class="container project-title">
        <div class="row">
            <h2>{{ $project->title }}</h2>


            @foreach ($photos as $photo)
            <div class="col-md-4">

                <h2>{{ $photo->number }}</h2>
                <a href="{{route('photos.show', $photo->id)}}"
                <p>
                <img src="{{ $photo->photo }}">
                </p>
            
            @endforeach
        </div>
    </div>
@endsection