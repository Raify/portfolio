@extends('layouts.main')
@section('content')



<div class=container>
		<div class="row">
            <div class="box">
                <div class="col-lg-12">

						@if(isset($project))
						<form action="{{route('projects.update', $project->id)}}" method=POST>
						<input type="hidden" name="_method" value="PUT">
						@else
						<form method=POST action="{{route('projects.store')}}">
						@endif

								{{ csrf_field() }}

								<label> Project:</label><br>

								@if(isset($project))
								<input   value="{{$project['title']}}" class="form-control" type="text" name="title" ><br>
								@else

								<input   class="form-control" type="text" name="title" ><br>
								@endif

								<label>Image URL:</label><br>
			@if(isset($project))
		<input  value="{{$project['photo']}}" class="form-control" type="text" name="photo">
		@else
		<input class="form-control" type="text" name="photo">
		@endif
		
         <label>Description:</label><br>
       	@if(isset($project))
		<textarea class ="form-control" name="description" cols=30 rows=3>{{$project['description']}}</textarea><br>
		@else
		<textarea class ="form-control" name="description" cols=30 rows=3></textarea><br>
		@endif
								


	<button class="btn btn-primary">Add project</button>

</form>	

@if(isset($project))
<form method=POST action="{{route('projects.destroy', $project->id)}}">

<input type="hidden" name="_method" value="DELETE">
{{ csrf_field() }}
<button class="btn btn-danger">Delete</button>
</form>
@endif
<!-- Paslepta forma -->
</div>
					
			
		</div>
	</div>		
</div>
<!-- Paslepta forma -->

@endsection