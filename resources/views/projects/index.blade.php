@extends("layouts.main")
@section("content")
      @yield('content')
	    
 <div class=container>
 		<h3>Projects</h3> 
		<a href="{{ route('projects.create') }}"  class="btn btn-danger btn-sm">Add New project</a><br>

		@foreach($projects as $project)
		<div class="row border-bottom">
           
                <div class="col-md-6 col-sm-12">
					
					<span>{{ $project['title']}}</span><br>
		 		 		<a href="{{route('projects.show', $project->id)}}"   class="btn btn-primary btn-sm">project photos</a>
		 		 		<p>{{ $project['description']}}</p><br>
				 		
				 		<a href="{{route('projects.edit', $project->id)}}" class="btn btn-success btn-sm">Edit</a>					
			    </div>
			    <div class="col-md-6 col-sm-12">
					<img src="{{$project['photo']}}" class="row img-thumbnail">
			    </div>
	    </div><br>	
	    	@endforeach
<div>		
@endsection