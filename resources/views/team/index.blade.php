@extends('layouts.main')

@section('content')
<div class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="text-center text-primary">Team</h1>
                        <p class="text-center">Inspiration comes from surroundings</p>
                    </div>
                </div>
                <div class="row">
                
                
                    <div class="col-md-4">
                        <img src="photo_uploads/nature2.jpg"
                        class="img-quadrata center-block circular image-responsive" >
                        <h3 class="text-center" >John Doe
                        <br/>
                        <small> Developer</small></h3>
                    </div>
                    <div class="col-md-4">
                        <img src="photo_uploads/nature2.jpg"
                        class="center-block img-quadrata circular" >
                        <h3 class="text-center" >John Smith
                        <br/>
                        <small> Web Designer</small></h3>
                    </div>
                    <div class="col-md-4">
                        <img src="photo_uploads/nature2.jpg"
                        class="center-block img-quadrata circular">
                        <h3 class="text-center">Sara James
                        <br/>
                        <small> Content manager</small></h3>
                    </div>
                </div>
            </div>
        </div>

@endsection